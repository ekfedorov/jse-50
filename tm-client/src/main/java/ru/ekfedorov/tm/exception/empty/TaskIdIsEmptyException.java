package ru.ekfedorov.tm.exception.empty;

import ru.ekfedorov.tm.exception.AbstractException;

public final class TaskIdIsEmptyException extends AbstractException {

    public TaskIdIsEmptyException() {
        super("Error! TaskId - is empty...");
    }

}
