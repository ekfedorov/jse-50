package ru.ekfedorov.tm.exception.empty;

import ru.ekfedorov.tm.exception.AbstractException;

public final class LoginIsEmptyException extends AbstractException {

    public LoginIsEmptyException() {
        super("Error! Login is empty...");
    }

}

