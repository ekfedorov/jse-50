package ru.ekfedorov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Close application.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "exit";
    }

    @Override
    public void execute() {
        if (bootstrap == null) return;
        bootstrap.parseCommand("logout");
        System.exit(0);
    }

}
