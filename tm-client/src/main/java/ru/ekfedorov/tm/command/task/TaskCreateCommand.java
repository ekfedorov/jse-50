package ru.ekfedorov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.command.AbstractTaskCommand;
import ru.ekfedorov.tm.endpoint.Session;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.util.TerminalUtil;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Create new task.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-create";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (bootstrap == null) throw new NullObjectException();
        @Nullable final Session session = bootstrap.getSession();
        if (endpointLocator == null) throw new NullObjectException();
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        @NotNull String name = TerminalUtil.nextLine();
        if (isEmpty(name)) name = "task";
        System.out.println("ENTER DESCRIPTION:");
        @NotNull String description = TerminalUtil.nextLine();
        if (isEmpty(description)) description = "without description";
        endpointLocator.getTaskEndpoint().addTask(session, name, description);
        System.out.println();
    }

}
