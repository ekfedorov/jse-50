package ru.ekfedorov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.model.AbstractGraphEntity;

import java.util.List;
import java.util.Optional;

public interface IGraphService<E extends AbstractGraphEntity> {

    void add(@NotNull E entity);

    void addAll(@NotNull List<E> entities);

    void clear();

    boolean contains(@NotNull String id);

    @NotNull
    List<E> findAll();

    @NotNull
    List<E> findAllSort(@NotNull String sort);

    @NotNull
    Optional<E> findOneById(@Nullable String id);

    void removeOneById(@Nullable String id);

}
